from fastapi import FastAPI
from pydantic import BaseModel
from deta import Deta
from esewa.esewa import verification

deta = Deta("a0jaojyy_3z6ceTjyfr2hPyFBJMiwnU8nWJ71igNj")
db = deta.Base("bank_verification")

bank_verify = FastAPI()

@bank_verify.post("/verify/")
def user_verification(bank_code:str,account_number:str,name:str):
    if next(db.fetch({"account_number":account_number,"bank_code":bank_code,"name":name})):
        # print('from db')
        return {"status":0}

    else:
        response = verification.verify(bank_code,account_number,name)
        if response['code'] == 0:
            db.put({'name':name,'bank_code':bank_code,'account_number':account_number})
            # print('from esewa')
            return {"status":0}
        else:
            return {"status":-1}
